// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.


function convertString(arr){
    let ans =""
    if (arr.length===0){
        ans = ""
    }else{
        for (let word=0;word<arr.length;word++){
            ans=ans+arr[word]+" "
        }
    }
    return ans
}

module.exports = convertString