// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

function printMonth(str){
    let ans = str.split("/")
    return parseInt(ans[0])
}

module.exports = printMonth