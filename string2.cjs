// ==== String Problem #2 ====
/*Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and 
return it in an array in numeric values. [111, 139, 161, 143].*/

// Support IPV4 addresses only. If there are other characters detected, return an empty array.

function convertIpAddress(add){
    let splitArray = add.split(".")
    let newArray = [];
    for (let num in splitArray){
        if (isNaN(splitArray[num])){
            return []
        }else{
            newArray.push(parseInt(splitArray[num]))
        }
    }
    return newArray
    
}

module.exports=convertIpAddress