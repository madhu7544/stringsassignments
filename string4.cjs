// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function fullName(str){
    let ans=""
    if(str.first_name){
        let first = ""
        for (let chr in str.first_name){
            if (chr == 0){
                first= first+ str.first_name[chr].toUpperCase()
            }
            else{
                first=first+str.first_name[chr].toLowerCase()
            }
        }
        ans =ans+ first+" "
    }
    if(str.middle_name){
        let middle = ""
        for (let chr in str.middle_name){
            if (chr == 0){
                middle= middle+ str.middle_name[chr].toUpperCase()
            }
            else{
                middle=middle+str.middle_name[chr].toLowerCase()
            }
        }
        ans =ans+ middle+" "
    }
    if(str.last_name){
        let last = ""
        for (let chr in str.last_name){
            if (chr == 0){
                last= last+ str.last_name[chr].toUpperCase()
            }
            else{
                last=last+str.last_name[chr].toLowerCase()
            }
        }
        ans =ans+ last+" "
    }

    return ans
}

module.exports=fullName